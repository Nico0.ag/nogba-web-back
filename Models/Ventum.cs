﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Web.Models
{
    public partial class Ventum
    {
        public Ventum()
        {
            VentaProductos = new HashSet<VentaProducto>();
            VentaTrabajos = new HashSet<VentaTrabajo>();
        }

        public int IdVenta { get; set; }
        public int? IdCliente { get; set; }
        public DateTime? Fecha { get; set; }

        public virtual Vendedore IdCliente1 { get; set; }
        public virtual Cliente IdClienteNavigation { get; set; }
        public virtual ICollection<VentaProducto> VentaProductos { get; set; }
        public virtual ICollection<VentaTrabajo> VentaTrabajos { get; set; }
    }
}
