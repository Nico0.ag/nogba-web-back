﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Web.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            Venta = new HashSet<Ventum>();
        }

        public int IdClientes { get; set; }
        public string NombreApe { get; set; }
        public int? Telefono { get; set; }
        public int? Estado { get; set; }

        public virtual ICollection<Ventum> Venta { get; set; }
    }
}
