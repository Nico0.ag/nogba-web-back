﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Web.Models
{
    public partial class Producto
    {
        public Producto()
        {
            VentaProductos = new HashSet<VentaProducto>();
        }

        public int IdProductos { get; set; }
        public string Nombre { get; set; }
        public decimal? Valor { get; set; }
        public int? Estado { get; set; }

        public virtual ICollection<VentaProducto> VentaProductos { get; set; }
    }
}
