﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Web.Models
{
    public partial class TipoTrabajo
    {
        public TipoTrabajo()
        {
            VentaTrabajos = new HashSet<VentaTrabajo>();
        }

        public int IdTrabajos { get; set; }
        public string TipoDeTrabajo { get; set; }
        public decimal? Valor { get; set; }
        public int? Estado { get; set; }

        public virtual ICollection<VentaTrabajo> VentaTrabajos { get; set; }
    }
}
