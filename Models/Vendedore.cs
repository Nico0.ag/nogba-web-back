﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Web.Models
{
    public partial class Vendedore
    {
        public Vendedore()
        {
            Venta = new HashSet<Ventum>();
        }

        public int IdVendedores { get; set; }
        public string Nombre { get; set; }
        public int? Estado { get; set; }

        public virtual ICollection<Ventum> Venta { get; set; }
    }
}
