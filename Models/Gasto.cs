﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Web.Models
{
    public partial class Gasto
    {
        public int IdGastos { get; set; }
        public string Vendedor { get; set; }
        public decimal? Valor { get; set; }
        public string Descripcion { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
