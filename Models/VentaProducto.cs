﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Web.Models
{
    public partial class VentaProducto
    {
        public int IdVentProd { get; set; }
        public int? IdVenta { get; set; }
        public int? IdProducto { get; set; }
        public int? Cantidad { get; set; }
        public decimal? Precio { get; set; }

        public virtual Producto IdProductoNavigation { get; set; }
        public virtual Ventum IdVentaNavigation { get; set; }
    }
}
