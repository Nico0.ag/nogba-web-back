﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Web.Models
{
    public partial class NogbaContext : DbContext
    {
        public NogbaContext()
        {
        }

        public NogbaContext(DbContextOptions<NogbaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<Gasto> Gastos { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<TipoTrabajo> TipoTrabajos { get; set; }
        public virtual DbSet<Vendedore> Vendedores { get; set; }
        public virtual DbSet<VentaProducto> VentaProductos { get; set; }
        public virtual DbSet<VentaTrabajo> VentaTrabajos { get; set; }
        public virtual DbSet<Ventum> Venta { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=MPC518;Initial Catalog=Nogba;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasKey(e => e.IdClientes);

                entity.Property(e => e.IdClientes).HasColumnName("Id_Clientes");

                entity.Property(e => e.NombreApe)
                    .HasMaxLength(25)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Gasto>(entity =>
            {
                entity.HasKey(e => e.IdGastos);

                entity.Property(e => e.IdGastos).HasColumnName("Id_Gastos");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsFixedLength(true);

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.Valor).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Vendedor)
                    .HasMaxLength(50)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasKey(e => e.IdProductos);

                entity.Property(e => e.IdProductos).HasColumnName("Id_Productos");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsFixedLength(true);

                entity.Property(e => e.Valor).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<TipoTrabajo>(entity =>
            {
                entity.HasKey(e => e.IdTrabajos);

                entity.ToTable("TipoTrabajo");

                entity.Property(e => e.IdTrabajos).HasColumnName("Id_Trabajos");

                entity.Property(e => e.TipoDeTrabajo)
                    .HasMaxLength(50)
                    .IsFixedLength(true);

                entity.Property(e => e.Valor).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<Vendedore>(entity =>
            {
                entity.HasKey(e => e.IdVendedores);

                entity.Property(e => e.IdVendedores).HasColumnName("Id_Vendedores");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<VentaProducto>(entity =>
            {
                entity.HasKey(e => e.IdVentProd);

                entity.ToTable("VentaProducto");

                entity.Property(e => e.IdVentProd).HasColumnName("Id_VentProd");

                entity.Property(e => e.Cantidad).HasColumnName("cantidad");

                entity.Property(e => e.IdProducto).HasColumnName("id_producto");

                entity.Property(e => e.IdVenta).HasColumnName("id_venta");

                entity.Property(e => e.Precio)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("precio");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.VentaProductos)
                    .HasForeignKey(d => d.IdProducto)
                    .HasConstraintName("FK_VentaProducto_Productos");

                entity.HasOne(d => d.IdVentaNavigation)
                    .WithMany(p => p.VentaProductos)
                    .HasForeignKey(d => d.IdVenta)
                    .HasConstraintName("FK_VentaProducto_Venta");
            });

            modelBuilder.Entity<VentaTrabajo>(entity =>
            {
                entity.HasKey(e => e.IdVentTrab);

                entity.ToTable("VentaTrabajo");

                entity.Property(e => e.IdVentTrab).HasColumnName("id_VentTrab");

                entity.Property(e => e.Cantidad).HasColumnName("cantidad");

                entity.Property(e => e.IdTrabajo).HasColumnName("id_trabajo");

                entity.Property(e => e.IdVenta).HasColumnName("id_venta");

                entity.Property(e => e.Precio)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("precio");

                entity.HasOne(d => d.IdTrabajoNavigation)
                    .WithMany(p => p.VentaTrabajos)
                    .HasForeignKey(d => d.IdTrabajo)
                    .HasConstraintName("FK_VentaTrabajo_TipoTrabajo");

                entity.HasOne(d => d.IdVentaNavigation)
                    .WithMany(p => p.VentaTrabajos)
                    .HasForeignKey(d => d.IdVenta)
                    .HasConstraintName("FK_VentaTrabajo_Venta");
            });

            modelBuilder.Entity<Ventum>(entity =>
            {
                entity.HasKey(e => e.IdVenta);

                entity.Property(e => e.IdVenta).HasColumnName("Id_Venta");

                entity.Property(e => e.Fecha)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha");

                entity.Property(e => e.IdCliente).HasColumnName("id_cliente");

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.Venta)
                    .HasForeignKey(d => d.IdCliente)
                    .HasConstraintName("FK_Venta_Clientes");

                entity.HasOne(d => d.IdCliente1)
                    .WithMany(p => p.Venta)
                    .HasForeignKey(d => d.IdCliente)
                    .HasConstraintName("FK_Venta_Vendedores");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
