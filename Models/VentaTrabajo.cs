﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Web.Models
{
    public partial class VentaTrabajo
    {
        public int IdVentTrab { get; set; }
        public int? IdVenta { get; set; }
        public int? IdTrabajo { get; set; }
        public int? Cantidad { get; set; }
        public decimal? Precio { get; set; }

        public virtual TipoTrabajo IdTrabajoNavigation { get; set; }
        public virtual Ventum IdVentaNavigation { get; set; }
    }
}
