﻿using Microsoft.AspNetCore.Mvc;
using Nogba_Back.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Nogba_Back.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : Controller
    {
        
        private readonly NogbaContext context;
        public ProductosController(NogbaContext context)
        {
            this.context = context;
        } 

        [HttpGet ("Getallprodu")] //solo campos seleccionados
        public IActionResult Getallprodu()
        {
            try {
                var status = context.Productos.Where(a => a.Estado == 1).Select(p => new
                {
                    id_Productos = p.IdProductos,
                    Nombre = p.Nombre,
                    Valor = p.Valor,
                }).ToList();
                return Ok(status);
            }
            catch
            {
                return BadRequest();
            }
        }


        [HttpGet("{id}")] //campos filtrados contains id, se edita aca (p => p.IdProductos == id)
        public IActionResult Getproduid(int id)
        {            
            var producto = context.Productos.Where(p => p.IdProductos == id).Select( p => new 
            {
                id_Productos = p.IdProductos,
                Nombre = p.Nombre,
                Valor = p.Valor
            }).ToList();
            return Ok(producto);
        }

        [HttpDelete("{id}")] //OJO QUE ESTA RELACIONADO, SI BORRA ACA BORRA EN VENTAPRODUCTO!!! OJO AL TEJOOOOOO
        public async Task<ActionResult>  DeleteProductos(int id)
        {
            var DeleteProductos = context.Productos.FirstOrDefault(e => e.IdProductos == id);
            if (DeleteProductos != null)
            {
                context.Productos.Remove(DeleteProductos);
                await context.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }
      
        [HttpPost("nuevoproducto")] //agregar status en 1 para dar de alta un producto desde el back
        public ActionResult PostClientes(ProductoDTO produ)
        {
            var nuevoprodu = new Producto() { Nombre = produ.Nombre, Valor = produ.Valor, Estado = 1};
            context.Productos.Add(nuevoprodu);
            context.SaveChangesAsync();
            return Ok();
        }
    }
}

 