﻿using Microsoft.AspNetCore.Mvc;
using Nogba_Back.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Nogba_Back.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendedoresController : Controller
    {
        private readonly NogbaContext context;
        public VendedoresController(NogbaContext context)
        {
            this.context = context;
        }

        [HttpGet("getallvendedores")] //solo campos seleccionados
        public IActionResult getallvendedores()
        {
            try
            {
                var status = context.Vendedores.Where(a => a.Estado == 1).Select(p => new
                {
                    idvendedor = p.IdVendedores,
                    nombre = p.Nombre,
                }).ToList();
                return Ok(status);
            }
            catch
            {
                return BadRequest();
            }
        }

    }
}
    
 