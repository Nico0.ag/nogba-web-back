﻿using Microsoft.AspNetCore.Mvc;
using Nogba_Back.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTO;
 
using Web.Models;

namespace Nogba_Back.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrabajosController : Controller
    {
        private readonly NogbaContext context;
        public TrabajosController(NogbaContext context)
        {
            this.context = context;
        }

        [HttpGet("Getalltra")] //solo campos seleccionados
        public IActionResult Getalltra()
        {
            try
            {
                var status = context.TipoTrabajos.Where(a => a.Estado == 1).Select(p => new
                {
                    IdTrabajos = p.IdTrabajos,
                    TipoDeTrabajo = p.TipoDeTrabajo,
                    Valor = p.Valor
                }).ToList();
                return Ok(status);
            }
            catch {
                return BadRequest();
            
            }
        }
        
   

        [HttpGet("{id}")]
        public TipoTrabajo GetTrabajosID(int id)
        {
            var trabajos = context.TipoTrabajos.FirstOrDefault(a => a.IdTrabajos == id);
            return trabajos;
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult<TipoTrabajo>> DeleteTrabajos(int id)
        {
            var DeleteTrabajos = context.TipoTrabajos.FirstOrDefault(e => e.IdTrabajos == id);
            if (DeleteTrabajos != null)
            {
                context.TipoTrabajos.Remove(DeleteTrabajos);
                await context.SaveChangesAsync();
                return DeleteTrabajos;
            }
            return null;
        }

        
    }
}
