﻿using Microsoft.AspNetCore.Mvc;

using Nogba_Back.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTO;
 
using Web.Models;

namespace Nogba_Back.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : Controller
    {
        private readonly NogbaContext context;
        public ClientesController(NogbaContext context)
        {
            this.context = context;
        }

        [HttpGet("Getallcli")] //solo campos seleccionados
        public IActionResult Getallcli() { 
        try
        {        
            var cliente = context.Clientes.Where(p => p.Estado == 1).Select(a => new {
                IdCliente = a.IdClientes,
                NombreApe = a.NombreApe,
                Telefono = a.Telefono
            }).ToList();
            return Ok(cliente);
        }
        catch{
                return BadRequest();
        }
        }

        [HttpPost("nuevocliente")]
        public ActionResult PostClientes(ClienteDTO cliente)
        {
            var nuevocliente = new Cliente() { NombreApe = cliente.NombreApe, Telefono = cliente.Telefono };
            context.Clientes.Add(nuevocliente);
            context.SaveChangesAsync();
            return Ok();
        }
    }
}
 