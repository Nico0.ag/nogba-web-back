﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTO;
using Web.Models;
 

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentaTrabajoController : ControllerBase
    {
        private readonly NogbaContext context;
        public VentaTrabajoController(NogbaContext context)
        {
            this.context = context;
        }
 
            [HttpPost("venta")]
            public async Task<ActionResult> PostVentaTra(VentraDTO venta)
            {
                var ventacreada = new Ventum() { Fecha = DateTime.Now, IdCliente = venta.IdCliente};
                context.Venta.Add(ventacreada);
                foreach (VentatDTO ventatrabajo in venta.Trabajos)
                { 
                    ventacreada.VentaTrabajos.Add(new VentaTrabajo()
                    {
                        IdVentTrab = venta.idVentTrab,
                        Precio = venta.precio,
                        Cantidad = ventatrabajo.cantidad,
                        IdTrabajo = ventatrabajo.idtrabajo

                    });                
                }
                await context.SaveChangesAsync();   //podes guardar todo si tenes las relaciones hechas          
                return Ok();
            }      
    }
}
 