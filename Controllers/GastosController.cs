﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 
using Web.Models;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GastosController : Controller
    {
        private readonly NogbaContext context;
        public GastosController(NogbaContext context)
        {
            this.context = context;
        }

        [HttpGet("getallgastos")]
        public IActionResult getallgastos()
        {
            var dato = context.Venta.Select(p => new
            {
                id = p.IdVenta,
                idcli = p.IdCliente,
                fecha = p.Fecha
            }).ToList();
            return Ok(dato);
        }

        [HttpPost("postgastos")]
        public async Task<ActionResult> PostGastos(GastosDTO gastos)
        {
            var gastocreados = new Gasto() { IdGastos = gastos.IdGastos, Fecha = DateTime.Now, Descripcion = gastos.Descripcion,
            Valor = gastos.Valor, Vendedor = gastos.Vendedor };
            context.Gastos.Add(gastocreados);
            await context.SaveChangesAsync();
            return Ok();

        }
    }
}
 