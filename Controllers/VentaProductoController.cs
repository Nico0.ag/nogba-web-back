﻿using Microsoft.AspNetCore.Mvc;
using Nogba_Back.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;
 

namespace Nogba_Back.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentaProductosController : Controller
    {
        private readonly NogbaContext context;
        public VentaProductosController(NogbaContext context)
        {
            this.context = context;
        }
      
        [HttpGet("getultiventa ")] 
        public IActionResult getultiventa()
        {
            var dato = context.Venta.Select(p => new
            {
                id = p.IdVenta,
                idcli = p.IdCliente,
                fecha = p.Fecha
            }).ToList();
            return Ok(dato);             
        }
        
        [HttpPost("venta")]
        public async Task<ActionResult> PostVentaProdu(VentumDTO venta)
        {
            var ventacreada = new Ventum() { Fecha = DateTime.Now, IdCliente = venta.IdCliente };
            context.Venta.Add(ventacreada);
            foreach (VentaDTO ventaProducto in venta.Productos) 
            {
                ventacreada.VentaProductos.Add(new VentaProducto()
                {             
                    IdVentProd = venta.idventapro,
                    Precio = venta.precio,
                    Cantidad = ventaProducto.cantidad,
                    IdProducto = ventaProducto.idproducto
                    
                });
            }
            await context.SaveChangesAsync();   //podes guardar todo si tenes las relaciones hechas          
            return Ok();            
            //var ventacreada = new Ventum() { Fecha = DateTime.Now, IdCliente = venta.IdCliente };
            //context.Venta.Add(ventacreada);
            //await context.SaveChangesAsync();                    OPCION SIN RELACIONES HECHAS
            //var dato = ventacreada.IdVenta;
            //var dato = context.Venta.OrderBy(dat => dat.IdVenta).Select(x => x.IdVenta);//solicita SOLO el ultimo idventa 
            //  foreach (VentaDTO ventaProducto in venta.Productos ) //sacar idprodu y cantidad
            //  {                                      
            //      context.VentaProductos.Add(new VentaProducto() { IdVenta = dato, IdVentProd = venta.idventapro,
            //      Precio = venta.precio, Cantidad = ventaProducto.cantidad, IdProducto = ventaProducto.idproducto});
            //  }                        
            //await context.SaveChangesAsync();
            //return Ok();                        
        }      
    }
}
 