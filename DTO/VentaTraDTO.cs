﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTO
{    
    public class VentraDTO
    {
       public int idVentTrab { get; set; } //lo da el back
       public int IdVenta { get; set; }   //lo da el back //venta
       public DateTime? Fecha { get; set; } //lo da el back //venta
       public int? IdCliente { get; set; }                  //venta
       public int precio { get; set; }            
       public VentatDTO[] Trabajos { get; set; }
    }
    public class VentatDTO
    {
     public int cantidad { get; set; }
     public int idtrabajo { get; set; }
    }
     
}
