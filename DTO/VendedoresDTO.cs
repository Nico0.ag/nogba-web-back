﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTO
{
    public class VendedoresDTO
    {
        public int IdVendedores { get; set; }
        public string Nombre { get; set; }
    }
}
