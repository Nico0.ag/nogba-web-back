﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class ProductoDTO
    {
        public int IdProductos { get; set; }
        public string Nombre { get; set; }
        public decimal? Valor { get; set; }

    }
}
