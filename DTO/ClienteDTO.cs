﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTO
{
    public class ClienteDTO
    {
        public int IdClientes { get; set; }
        public string NombreApe { get; set; }
        public int Telefono { get; set; }
    }
}
