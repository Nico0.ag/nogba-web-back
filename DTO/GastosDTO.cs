﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class GastosDTO
    {
        public int IdGastos { get; set; }
        public string Vendedor { get; set; }
        public decimal? Valor { get; set; }
        public string Descripcion { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
