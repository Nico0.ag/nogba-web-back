﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Nogba_Back.Models
{
    public class VentumDTO
    {
        public int idventapro { get; set; } //lo da el back
        public int IdVenta { get; set; }   //lo da el back
        public int? IdCliente { get; set; }
        public int precio { get; set; }
        public DateTime? Fecha { get; set; } //lo da el back
        public VentaDTO[] Productos { get; set; }
    }
    public class VentaDTO
    {        
        public int cantidad { get; set; }
        public int idproducto { get; set; }        
    }
     
    
}
