﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTO
{
    public class TipoTrabajoDTO
    {
        public int IdTrabajos { get; set; }
        public string TipoDeTrabajo { get; set; }
        public decimal? Valor { get; set; }
    }
}
